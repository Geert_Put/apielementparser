<?php

declare(strict_types=1);

namespace Phpro\ApiElementParser;

use Phpro\ApiElementParser\Element\AnnotationElement;
use Phpro\ApiElementParser\Element\AssetElement;
use Phpro\ApiElementParser\Element\BasicAuthenticationSchemeElement;
use Phpro\ApiElementParser\Element\CategoryElement;
use Phpro\ApiElementParser\Element\CopyElement;
use Phpro\ApiElementParser\Element\DataStructureElement;
use Phpro\ApiElementParser\Element\ExtensionElement;
use Phpro\ApiElementParser\Element\HrefVariablesElement;
use Phpro\ApiElementParser\Element\HttpHeadersElement;
use Phpro\ApiElementParser\Element\HttpRequestElement;
use Phpro\ApiElementParser\Element\HttpResponseElement;
use Phpro\ApiElementParser\Element\HttpTransactionElement;
use Phpro\ApiElementParser\Element\OAuth2SchemeElement;
use Phpro\ApiElementParser\Element\ParseResultElement;
use Phpro\ApiElementParser\Element\ResourceElement;
use Phpro\ApiElementParser\Element\SourceMapElement;
use Phpro\ApiElementParser\Element\TokenAuthenticationSchemeElement;
use Phpro\ApiElementParser\Element\TransitionElement;
use Phpro\RefractParser\Element\BaseElement;
use Phpro\RefractParser\Parser as RefractParser;

/**
 * Class Parser
 *
 * @package Phpro\ApiElementParser
 */
final class Parser
{
    private const CUSTOM_PARSE_ELEMENTS = [
        [
            'className' => HrefVariablesElement::class,
            'elementName' => HrefVariablesElement::ELEMENT,
        ],
        [
            'className' => DataStructureElement::class,
            'elementName' => DataStructureElement::ELEMENT,
        ],
        [
            'className' => AssetElement::class,
            'elementName' => AssetElement::ELEMENT,
        ],
        [
            'className' => ResourceElement::class,
            'elementName' => ResourceElement::ELEMENT,
        ],
        [
            'className' => TransitionElement::class,
            'elementName' => TransitionElement::ELEMENT,
        ],
        [
            'className' => CategoryElement::class,
            'elementName' => CategoryElement::ELEMENT,
        ],
        [
            'className' => CopyElement::class,
            'elementName' => CopyElement::ELEMENT,
        ],
        [
            'className' => HttpTransactionElement::class,
            'elementName' => HttpTransactionElement::ELEMENT,
        ],
        [
            'className' => HttpHeadersElement::class,
            'elementName' => HttpHeadersElement::ELEMENT,
        ],
        [
            'className' => HttpRequestElement::class,
            'elementName' => HttpRequestElement::ELEMENT,
        ],
        [
            'className' => HttpResponseElement::class,
            'elementName' => HttpResponseElement::ELEMENT,
        ],
        [
            'className' => ParseResultElement::class,
            'elementName' => ParseResultElement::ELEMENT,
        ],
        [
            'className' => AnnotationElement::class,
            'elementName' => AnnotationElement::ELEMENT,
        ],
        [
            'className' => SourceMapElement::class,
            'elementName' => SourceMapElement::ELEMENT,
        ],
        [
            'className' => BasicAuthenticationSchemeElement::class,
            'elementName' => BasicAuthenticationSchemeElement::ELEMENT,
        ],
        [
            'className' => TokenAuthenticationSchemeElement::class,
            'elementName' => TokenAuthenticationSchemeElement::ELEMENT,
        ],
        [
            'className' => OAuth2SchemeElement::class,
            'elementName' => OAuth2SchemeElement::ELEMENT,
        ],
        [
            'className' => ExtensionElement::class,
            'elementName' => ExtensionElement::ELEMENT,
        ],
    ];

    /**
     * Parse a php object to an instance of BaseElement or one of the Api-Elements Elements that extend BaseElement
     *
     * @param string $json
     *
     * @return BaseElement
     *
     * @throws \Phpro\RefractParser\Exception\ParserException
     */
    final public static function parse(string $json): BaseElement
    {
        $refractParser = new RefractParser(self::CUSTOM_PARSE_ELEMENTS);

        return $refractParser->parse($json);
    }
}
