<?php

declare(strict_types=1);

namespace Phpro\ApiElementParser\Element;

use Phpro\RefractParser\Exception\AttributeParserException;
use Phpro\RefractParser\Exception\ContentParserException;
use Phpro\RefractParser\Exception\ParserException;
use Phpro\RefractParser\Element\BaseElement;
use Phpro\RefractParser\Element\Primitive\StringElement;

/**
 * Class HttpRequestElement
 *
 * @package Phpro\ApiElementParser\Element
 */
final class HttpRequestElement extends BaseElement
{

    public const ELEMENT = 'httpRequest';

    /**
     * HttpRequestElement constructor.
     *
     * @param BaseElement $baseElement
     *
     * @throws \Phpro\RefractParser\Exception\ParserException
     * @throws \Phpro\RefractParser\Exception\AttributeParserException
     * @throws \Phpro\RefractParser\Exception\ContentParserException
     */
    public function __construct(BaseElement $baseElement)
    {
        $content = $baseElement->getContent();
        $attributes = $baseElement->getAttributes();

        if ($attributes->hasAttribute(
                'headers'
            ) && !$attributes->getAttribute(
                    'headers'
                ) instanceof HttpHeadersElement) {
            throw new AttributeParserException(
                self::class,
                'headers',
                false,
                ['HttpHeadersElement']
            );
        }

        if ($attributes->hasAttribute(
                'method'
            ) && !$attributes->getAttribute(
                    'method'
                ) instanceof StringElement) {
            throw new AttributeParserException(
                self::class,
                'method',
                false,
                ['StringElement']
            );
        }

        if ($attributes->hasAttribute(
                'href'
            ) && !$attributes->getAttribute(
                    'href'
                ) instanceof StringElement) {
            throw new AttributeParserException(
                self::class,
                'href',
                false,
                ['StringElement']
            );
        }

        if ($attributes->hasAttribute(
                'hrefVariables'
            ) && !$attributes->getAttribute(
                    'hrefVariables'
                ) instanceof HrefVariablesElement) {
            throw new AttributeParserException(
                self::class,
                'hrefVariables',
                false,
                ['HrefVariablesElement']
            );
        }

        if ($content->getValue()) {
            if (!\is_array($content->getValue())) {
                throw new ContentParserException(
                    self::class,
                    $content->getValue(),
                    ['array']
                );
            }

            $numberOfDataStructureElements = 0;
            foreach ($content->getValue() as $element) {
                if (!($element instanceof CopyElement || $element instanceof DataStructureElement || $element instanceof AssetElement)) {
                    throw new ContentParserException(
                        self::class,
                        $content->getValue(),
                        [
                            'CopyElement[]',
                            'DataStructureElement[]',
                            'AssetElement[]',
                        ]
                    );
                }

                if ($element instanceof DataStructureElement) {
                    $numberOfDataStructureElements++;
                }
            }

            if ($numberOfDataStructureElements > 1) {
                throw new ParserException(
                    sprintf(
                        '%s : The content can only contain a single DataStructureElement',
                        self::class
                    )
                );
            }
        }

        parent::__construct(
            self::ELEMENT,
            $baseElement->getMeta(),
            $attributes,
            $content
        );
    }
}
