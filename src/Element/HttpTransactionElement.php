<?php

declare(strict_types=1);

namespace Phpro\ApiElementParser\Element;

use Phpro\RefractParser\Exception\AttributeParserException;
use Phpro\RefractParser\Exception\ContentParserException;
use Phpro\RefractParser\Exception\ParserException;
use Phpro\RefractParser\Element\BaseElement;
use Phpro\RefractParser\Element\Primitive\ArrayElement;

/**
 * Class HttpTransactionElement
 *
 * @package Phpro\ApiElementParser\Element
 */
final class HttpTransactionElement extends BaseElement
{

    public const ELEMENT = 'httpTransaction';

    /**
     * HttpTransactionElement constructor.
     *
     * @param BaseElement $baseElement
     *
     * @throws \Phpro\RefractParser\Exception\ParserException
     * @throws \Phpro\RefractParser\Exception\AttributeParserException
     * @throws \Phpro\RefractParser\Exception\ContentParserException
     */
    public function __construct(BaseElement $baseElement)
    {
        $attributes = $baseElement->getAttributes();
        $content = $baseElement->getContent();

        if ($attributes->hasAttribute(
                'authSchemes'
            ) && !$attributes->getAttribute(
                    'authSchemes'
                ) instanceof ArrayElement) {
            throw new AttributeParserException(
                self::class,
                'authSchemes',
                false,
                ['ArrayElement']
            );
        }

        if (!\is_array($content->getValue())) {
            throw new ContentParserException(
                self::class,
                $content->getValue(),
                ['array']
            );
        }

        $numberOfHttpRequestMessages = 0;
        $numberOfHttpResponseMessages = 0;
        foreach ($content->getValue() as $element) {
            if (!($element instanceof CopyElement || $element instanceof HttpRequestElement || $element instanceof HttpResponseElement)) {
                throw new ContentParserException(
                    self::class,
                    $content->getValue(),
                    [
                        'CopyElement[]',
                        'HttpRequestElement[]',
                        'HttpResponseElement[]',
                    ]
                );
            }

            if ($element instanceof HttpRequestElement) {
                $numberOfHttpRequestMessages++;
            }

            if ($element instanceof HttpResponseElement) {
                $numberOfHttpResponseMessages++;
            }
        }

        if (!$numberOfHttpRequestMessages === 1) {
            throw new ParserException(
                sprintf(
                    '%s : The content must contain exactly one HttpRequestElement',
                    self::class
                )
            );
        }

        if (!$numberOfHttpResponseMessages === 1) {
            throw new ParserException(
                sprintf(
                    '%s : The content must contain exactly one HttpResponseElement',
                    self::class
                )
            );
        }

        parent::__construct(
            self::ELEMENT,
            $baseElement->getMeta(),
            $baseElement->getAttributes(),
            $baseElement->getContent()
        );
    }
}
