<?php

declare(strict_types=1);

namespace Phpro\ApiElementParser\Element;

use Phpro\RefractParser\Element\Primitive\ObjectElement;

/**
 * Class BasicAuthenticationSchemeElement
 *
 * @package Phpro\ApiElementParser\Element
 */
final class BasicAuthenticationSchemeElement extends ObjectElement
{

    public const ELEMENT = 'Basic Authentication Scheme';
}
