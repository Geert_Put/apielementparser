<?php

declare(strict_types=1);

namespace Phpro\ApiElementParser\Element;

use Phpro\RefractParser\Exception\ContentParserException;
use Phpro\RefractParser\Exception\ParserException;
use Phpro\RefractParser\Element\BaseElement;
use Phpro\RefractParser\Element\Primitive\ArrayElement;
use Phpro\RefractParser\Element\Primitive\NumberElement;

/**
 * Class SourceMapElement
 *
 * @package Phpro\ApiElementParser\Element
 */
final class SourceMapElement extends BaseElement
{

    public const ELEMENT = 'sourceMap';

    /**
     * SourceMapElement constructor.
     *
     * @param BaseElement $baseElement
     *
     * @throws \Phpro\RefractParser\Exception\ParserException
     * @throws \Phpro\RefractParser\Exception\ContentParserException
     */
    public function __construct(BaseElement $baseElement)
    {
        $content = $baseElement->getContent();

        if ($content->getValue()) {
            if (!\is_array($content->getValue())) {
                throw new ContentParserException(
                    self::class,
                    $content->getValue(),
                    ['array']
                );
            }

            foreach ($content->getValue() as $element) {
                if (!$element instanceof ArrayElement) {
                    throw new ContentParserException(
                        self::class,
                        $content->getValue(),
                        ['ArrayElement[]']
                    );
                }
                foreach ($element->getContent()->getValue() as $nestedElement) {
                    if (!$nestedElement instanceof NumberElement) {
                        throw new ParserException(
                            vsprintf(
                                '%s : if the content is an ArrayElement, a %s is not allowed as an element. Allowed types are : %s',
                                [
                                    self::class,
                                    \gettype($nestedElement),
                                    ['NumberElement'],
                                ]
                            )
                        );
                    }
                }
            }
        }

        parent::__construct(
            self::ELEMENT,
            $baseElement->getMeta(),
            $baseElement->getAttributes(),
            $baseElement->getContent()
        );
    }
}
