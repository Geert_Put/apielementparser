<?php

declare(strict_types=1);

namespace Phpro\ApiElementParser\Element;

use Phpro\RefractParser\Exception\AttributeParserException;
use Phpro\RefractParser\Exception\ContentParserException;
use Phpro\RefractParser\Element\BaseElement;
use Phpro\RefractParser\Element\Primitive\ArrayElement;

/**
 * Class CategoryElement
 *
 * @package Phpro\ApiElementParser\Element
 */
final class CategoryElement extends BaseElement
{

    public const ELEMENT = 'category';

    /**
     * CategoryElement constructor.
     *
     * @param BaseElement $baseElement
     *
     * @throws \Phpro\RefractParser\Exception\ParserException
     * @throws \Phpro\RefractParser\Exception\AttributeParserException
     * @throws \Phpro\RefractParser\Exception\ContentParserException
     */
    public function __construct(BaseElement $baseElement)
    {
        $content = $baseElement->getContent();
        $attributes = $baseElement->getAttributes();

        if ($attributes->hasAttribute('metadata') && !$attributes->getAttribute(
                    'metadata'
                ) instanceof ArrayElement) {
            throw new AttributeParserException(
                self::class,
                'metadata',
                false,
                ['ArrayElement']
            );
        }

        if ($content->getValue()) {
            if (!\is_array($content->getValue())) {
                throw new ContentParserException(
                    self::class,
                    $content->getValue(),
                    ['array']
                );
            }
            foreach ($content->getValue() as $element) {
                if (!$element instanceof BaseElement) {
                    throw new ContentParserException(
                        self::class,
                        $content->getValue(),
                        ['BaseElement[]']
                    );

                }
            }
        }

        parent::__construct(
            self::ELEMENT,
            $baseElement->getMeta(),
            $baseElement->getAttributes(),
            $content
        );
    }
}
