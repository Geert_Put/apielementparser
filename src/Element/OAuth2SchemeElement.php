<?php

declare(strict_types=1);

namespace Phpro\ApiElementParser\Element;

use Phpro\RefractParser\Element\BaseElement;
use Phpro\RefractParser\Element\Primitive\MemberElement;
use Phpro\RefractParser\Exception\ContentParserException;
use Phpro\RefractParser\Exception\ParserException;

/**
 * Class OAuth2SchemeElement
 *
 * @package Phpro\ApiElementParser\Element
 */
final class OAuth2SchemeElement extends BaseElement
{

    public const ELEMENT = 'OAuth2 Scheme';

    /**
     * OAuth2SchemeElement constructor.
     *
     * @param BaseElement $baseElement
     *
     * @throws \Phpro\RefractParser\Exception\ParserException
     * @throws \Phpro\RefractParser\Exception\ContentParserException
     */
    public function __construct(BaseElement $baseElement)
    {
        $content = $baseElement->getContent();
        if ($content->getValue()) {
            if (!\is_array($content->getValue())) {
                throw new ContentParserException(
                    self::class,
                    $content->getValue(),
                    ['array']
                );
            }
            foreach ($content->getValue() as $element) {
                if (!($element instanceof MemberElement || $element instanceof TransitionElement)) {
                    throw new ContentParserException(
                        self::class,
                        $content->getValue(),
                        ['MemberElement[]', 'TransitionElement[]',]
                    );

                }
            }
        }

        parent::__construct(
            self::ELEMENT,
            $baseElement->getMeta(),
            $baseElement->getAttributes(),
            $baseElement->getContent()
        );
    }
}
