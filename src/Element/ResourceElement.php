<?php

declare(strict_types=1);

namespace Phpro\ApiElementParser\Element;

use Phpro\RefractParser\Exception\AttributeParserException;
use Phpro\RefractParser\Exception\ParserException;
use Phpro\RefractParser\Element\BaseElement;
use Phpro\RefractParser\Element\Primitive\StringElement;

/**
 * Class ResourceElement
 *
 * @package Phpro\ApiElementParser\Element
 */
final class ResourceElement extends BaseElement
{

    public const ELEMENT = 'resource';

    /**
     * ResourceElement constructor.
     *
     * @param BaseElement $baseElement
     *
     * @throws \Phpro\RefractParser\Exception\ParserException
     * @throws \Phpro\RefractParser\Exception\AttributeParserException;
     */
    public function __construct(BaseElement $baseElement)
    {
        $attributes = $baseElement->getAttributes();

        if (!$attributes->getAttribute(
                'href'
            ) instanceof StringElement) {
            throw new AttributeParserException(
                self::class,
                'href',
                true,
                ['StringElement']
            );
        }

        if ($attributes->hasAttribute(
                'hrefVariables'
            ) && !$attributes->getAttribute(
                    'hrefVariables'
                ) instanceof HrefVariablesElement) {
            throw new AttributeParserException(
                self::class,
                'hrefVariables',
                false,
                ['HrefVariablesElement']
            );
        }

        parent::__construct(
            self::ELEMENT,
            $baseElement->getMeta(),
            $baseElement->getAttributes(),
            $baseElement->getContent()
        );
    }
}
