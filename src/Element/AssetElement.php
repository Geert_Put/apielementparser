<?php

declare(strict_types=1);

namespace Phpro\ApiElementParser\Element;

use Phpro\RefractParser\Element\BaseElement;
use Phpro\RefractParser\Exception\MetaParserException;

/**
 * Class AssetElement
 *
 * @package Phpro\ApiElementParser\Element
 */
final class AssetElement extends BaseElement
{

    public const ELEMENT = 'asset';

    /**
     * AssetElement constructor.
     *
     * @param BaseElement $baseElement
     *
     * @throws \Phpro\RefractParser\Exception\ParserException
     * @throws \Phpro\RefractParser\Exception\MetaParserException
     */
    public function __construct(BaseElement $baseElement)
    {
        $metaClasses = $baseElement->getMeta()->getClasses();
        if ($metaClasses === null) {
            throw new MetaParserException(self::class, 'classes');
        }

        parent::__construct(
            self::ELEMENT,
            $baseElement->getMeta(),
            $baseElement->getAttributes(),
            $baseElement->getContent()
        );
    }
}
