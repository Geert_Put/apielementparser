<?php

declare(strict_types=1);

namespace Phpro\ApiElementParser\Element;

use Phpro\RefractParser\Exception\AttributeParserException;
use Phpro\RefractParser\Exception\ContentParserException;
use Phpro\RefractParser\Element\BaseElement;
use Phpro\RefractParser\Element\Primitive\ArrayElement;
use Phpro\RefractParser\Element\Primitive\StringElement;

/**
 * Class TransitionElement
 *
 * @package Phpro\ApiElementParser\Element
 */
final class TransitionElement extends BaseElement
{

    public const ELEMENT = 'transition';

    /**
     * TransitionElement constructor.
     *
     * @param BaseElement $baseElement
     *
     * @throws \Phpro\RefractParser\Exception\ParserException
     * @throws \Phpro\RefractParser\Exception\AttributeParserException
     * @throws \Phpro\RefractParser\Exception\AttributeParserException
     */
    public function __construct(BaseElement $baseElement)
    {
        $attributes = $baseElement->getAttributes();
        $content = $baseElement->getContent();

        if ($attributes->hasAttribute(
                'relation'
            ) && !$attributes->getAttribute(
                    'relation'
                ) instanceof StringElement) {
            throw new AttributeParserException(
                self::class,
                'relation',
                false,
                ['StringElement']
            );
        }

        if ($attributes->hasAttribute(
                'href'
            ) && !$attributes->getAttribute(
                    'href'
                ) instanceof StringElement) {
            throw new AttributeParserException(
                self::class,
                'href',
                false,
                ['StringElement']
            );
        }

        if ($attributes->hasAttribute(
                'hrefVariables'
            ) && !$attributes->getAttribute(
                    'hrefVariables'
                ) instanceof HrefVariablesElement) {
            throw new AttributeParserException(
                self::class,
                'hrefVariables',
                false,
                ['HrefVariablesElement']
            );
        }

        if ($attributes->hasAttribute(
                'data'
            ) && !$attributes->getAttribute(
                    'data'
                ) instanceof DataStructureElement) {
            throw new AttributeParserException(
                self::class,
                'data',
                false,
                ['DataStructureElement']
            );
        }

        if ($attributes->hasAttribute('contentTypes')) {
            if (!$attributes->getAttribute(
                    'contentTypes'
                ) instanceof ArrayElement) {
                throw new AttributeParserException(
                    self::class,
                    'contentTypes',
                    false,
                    ['ArrayElement']
                );
            }

            foreach ($attributes->getAttribute('contentTypes')
                         ->getContent()
                         ->getValue() as $contentType) {
                if (!$contentType instanceof StringElement) {
                    vsprintf(
                        '%s : if the attribute contentTypes is an ArrayElement, a %s is not allowed as an element. Allowed types are : %s',
                        [
                            self::class,
                            \gettype($contentType),
                            ['StringElement'],
                        ]
                    );
                }
            }
        }

        if ($content->getValue()) {
            if (!\is_array($content->getValue())) {
                throw new ContentParserException(self::class, $content->getValue(), ['array']);
            }

            foreach ($content->getValue() as $element) {
                if (!($element instanceof HttpTransactionElement || $element instanceof CopyElement)) {
                    throw new ContentParserException(
                        self::class,
                        $content->getValue(),
                        ['HttpTransactionElement[]', 'CopyElement[]']
                    );

                }
            }
        }

        parent::__construct(
            self::ELEMENT,
            $baseElement->getMeta(),
            $baseElement->getAttributes(),
            $baseElement->getContent()
        );
    }
}
