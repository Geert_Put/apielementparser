<?php

declare(strict_types=1);

namespace Phpro\ApiElementParser\Element;

use Phpro\RefractParser\Element\BaseElement;
use Phpro\RefractParser\Exception\ContentParserException;
use Phpro\RefractParser\Exception\ParserException;

/**
 * Class ParseResultElement
 *
 * @package Phpro\ApiElementParser\Element
 */
final class ParseResultElement extends BaseElement
{

    public const ELEMENT = 'parseResult';

    /**
     * ParseResultElement constructor.
     *
     * @param BaseElement $baseElement
     *
     * @throws \Phpro\RefractParser\Exception\ParserException
     * @throws \Phpro\RefractParser\Exception\ContentParserException
     */
    public function __construct(BaseElement $baseElement)
    {
        $content = $baseElement->getContent();
        if (!\is_array($content->getValue())) {
            throw new ContentParserException(
                self::class,
                $content->getValue(),
                ['array']
            );
        }

        foreach ($content->getValue() as $element) {
            if (!($element instanceof CategoryElement || $element instanceof AnnotationElement)) {
                throw new ContentParserException(
                    self::class,
                    $content->getValue(),
                    ['CategoryElement[]', 'AnnotationElement[]',]
                );
            }
        }

        parent::__construct(
            self::ELEMENT,
            $baseElement->getMeta(),
            $baseElement->getAttributes(),
            $baseElement->getContent()
        );
    }
}
