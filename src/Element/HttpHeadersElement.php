<?php

declare(strict_types=1);

namespace Phpro\ApiElementParser\Element;

use Phpro\RefractParser\Exception\ContentParserException;
use Phpro\RefractParser\Element\BaseElement;
use Phpro\RefractParser\Element\Primitive\ArrayElement;
use Phpro\RefractParser\Element\Primitive\MemberElement;

/**
 * Class HttpHeadersElement
 *
 * @package Phpro\ApiElementParser\Element
 */
final class HttpHeadersElement extends ArrayElement
{

    public const ELEMENT = 'httpHeaders';

    /**
     * HttpHeadersElement constructor.
     *
     * @param BaseElement $baseElement
     *
     * @throws \Phpro\RefractParser\Exception\ParserException
     * @throws \Phpro\RefractParser\Exception\ContentParserException
     */
    public function __construct(BaseElement $baseElement)
    {
        $content = $baseElement->getContent();

        if (!\is_array($content->getValue())) {
            throw new ContentParserException(
                self::class,
                $content->getValue(),
                ['array']
            );
        }

        foreach ($content->getValue() as $element) {
            if (!$element instanceof MemberElement) {
                throw new ContentParserException(
                    self::class,
                    $element,
                    ['MemberElement']
                );
            }
        }

        parent::__construct($baseElement);
    }
}
