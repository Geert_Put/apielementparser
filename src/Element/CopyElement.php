<?php

declare(strict_types=1);

namespace Phpro\ApiElementParser\Element;

use Phpro\RefractParser\Element\BaseElement;
use Phpro\RefractParser\Element\Primitive\StringElement;
use Phpro\RefractParser\Exception\AttributeParserException;
use Phpro\RefractParser\Exception\ContentParserException;

/**
 * Class CopyElement
 *
 * @package Phpro\ApiElementParser\Element
 */
final class CopyElement extends BaseElement
{

    public const ELEMENT = 'copy';

    /**
     * CopyElement constructor.
     *
     * @param BaseElement $baseElement
     *
     * @throws \Phpro\RefractParser\Exception\ParserException
     * @throws \Phpro\RefractParser\Exception\AttributeParserException
     * @throws \Phpro\RefractParser\Exception\ContentParserException
     */
    public function __construct(BaseElement $baseElement)
    {
        $content = $baseElement->getContent();
        $attributes = $baseElement->getAttributes();

        if ($attributes->hasAttribute(
                'contentType'
            ) && !$attributes->getAttribute(
                    'contentType'
                ) instanceof StringElement) {
            throw new AttributeParserException(
                self::class,
                'contentType',
                false,
                ['StringElement']
            );
        }

        if (!\is_string(
            $content->getValue()
        )) {
            throw new ContentParserException(
                self::class,
                $content->getValue(),
                ['string']
            );
        }

        parent::__construct(
            self::ELEMENT,
            $baseElement->getMeta(),
            $baseElement->getAttributes(),
            $baseElement->getContent()
        );
    }
}
