<?php

declare(strict_types=1);

namespace Phpro\ApiElementParser\Element;

use Phpro\RefractParser\Element\BaseElement;
use Phpro\RefractParser\Element\Primitive\MemberElement;
use Phpro\RefractParser\Exception\ContentParserException;
use Phpro\RefractParser\Exception\ParserException;

/**
 * Class BasicAuthenticationSchemeElement
 *
 * @package Phpro\ApiElementParser\Element
 */
final class TokenAuthenticationSchemeElement extends BaseElement
{

    public const ELEMENT = 'Token Authentication Scheme';

    /**
     * TokenAuthenticationSchemeElement constructor.
     *
     * @param BaseElement $baseElement
     *
     * @throws \Phpro\RefractParser\Exception\ParserException
     * @throws \Phpro\RefractParser\Exception\ContentParserException
     */
    public function __construct(BaseElement $baseElement)
    {
        $content = $baseElement->getContent();

        if ($content->getValue()) {
            if (!\is_array($content->getValue())) {
                throw new ContentParserException(
                    self::class,
                    $content->getValue(),
                    ['array']
                );
            }
            foreach ($content->getValue() as $element) {
                if (!$element instanceof MemberElement) {
                    throw new ContentParserException(
                        self::class,
                        $content->getValue(),
                        ['MemberElement[]']
                    );

                }
            }
        }

        parent::__construct(
            self::ELEMENT,
            $baseElement->getMeta(),
            $baseElement->getAttributes(),
            $baseElement->getContent()
        );
    }
}
