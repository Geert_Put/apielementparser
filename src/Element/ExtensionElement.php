<?php

declare(strict_types=1);

namespace Phpro\ApiElementParser\Element;

use Phpro\RefractParser\Element\BaseElement;

/**
 * Class ExtensionElement
 *
 * @package Phpro\ApiElementParser\Element
 */
final class ExtensionElement extends BaseElement
{

    public const ELEMENT = 'extension';

    /**
     * ExtensionElement constructor.
     *
     * @param BaseElement $baseElement
     *
     * @throws \Phpro\RefractParser\Exception\ParserException
     */
    public function __construct(BaseElement $baseElement)
    {
        parent::__construct(
            self::ELEMENT,
            $baseElement->getMeta(),
            $baseElement->getAttributes(),
            $baseElement->getContent()
        );
    }
}
