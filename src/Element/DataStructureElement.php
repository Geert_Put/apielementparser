<?php

declare(strict_types=1);

namespace Phpro\ApiElementParser\Element;

use Phpro\RefractParser\Element\BaseElement;

/**
 * Class DataStructureElement
 *
 * @package Phpro\ApiElementParser\Element
 */
final class DataStructureElement extends BaseElement
{
    public const ELEMENT = 'dataStructure';

    /**
     * SelectElement constructor.
     *
     * @param BaseElement $baseElement
     *
     * @throws \Phpro\RefractParser\Exception\ParserException
     */
    public function __construct(BaseElement $baseElement)
    {
        parent::__construct(
            self::ELEMENT,
            $baseElement->getMeta(),
            $baseElement->getAttributes(),
            $baseElement->getContent()
        );
    }
}
