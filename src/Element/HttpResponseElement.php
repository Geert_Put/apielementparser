<?php

declare(strict_types=1);

namespace Phpro\ApiElementParser\Element;

use Phpro\RefractParser\Exception\AttributeParserException;
use Phpro\RefractParser\Exception\ContentParserException;
use Phpro\RefractParser\Exception\ParserException;
use Phpro\RefractParser\Element\BaseElement;
use Phpro\RefractParser\Element\Primitive\NumberElement;
use Phpro\RefractParser\Element\Primitive\StringElement;

/**
 * Class HttpResponseElement
 *
 * @package Phpro\ApiElementParser\Element
 */
final class HttpResponseElement extends BaseElement
{

    public const ELEMENT = 'httpResponse';

    /**
     * HttpResponseElement constructor.
     *
     * @param BaseElement $baseElement
     *
     * @throws \Phpro\RefractParser\Exception\ParserException
     * @throws \Phpro\RefractParser\Exception\AttributeParserException
     * @throws \Phpro\RefractParser\Exception\ContentParserException
     */
    public function __construct(BaseElement $baseElement)
    {
        $attributes = $baseElement->getAttributes();
        $content = $baseElement->getContent();

        if ($attributes->hasAttribute(
                'headers'
            ) && !$attributes->getAttribute(
                    'headers'
                ) instanceof HttpHeadersElement) {
            throw new AttributeParserException(
                self::class,
                'headers',
                false,
                ['HttpHeadersElement']
            );
        }

        if ($attributes->hasAttribute(
                'statusCode'
            ) && !($attributes->getAttribute(
                    'statusCode'
                ) instanceof NumberElement || $attributes
                    ->getAttribute(
                        'statusCode'
                    ) instanceof StringElement)) {
            throw new AttributeParserException(
                self::class,
                'statusCode',
                false,
                ['StringElement', 'NumberElement']
            );
        }

        if ($baseElement->getContent()->getValue()) {
            if (!\is_array($baseElement->getContent()->getValue())) {
                throw new ContentParserException(
                    self::class,
                    $content->getValue(),
                    ['array']
                );
            }

            $numberOfDataStructureElements = 0;
            foreach ($content->getValue() as $element) {
                if (!($element instanceof CopyElement || $element instanceof DataStructureElement || $element instanceof AssetElement)) {
                    throw new ContentParserException(
                        self::class,
                        $content->getValue(),
                        [
                            'CopyElement[]',
                            'DataStructureElement[]',
                            'AssetElement[]',
                        ]
                    );
                }

                if ($element instanceof DataStructureElement) {
                    $numberOfDataStructureElements++;
                }
            }

            if ($numberOfDataStructureElements > 1) {
                throw new ParserException(
                    sprintf(
                        '%s : The content can only contain a single DataStructureElement',
                        self::class
                    )
                );
            }
        }

        parent::__construct(
            self::ELEMENT,
            $baseElement->getMeta(),
            $baseElement->getAttributes(),
            $baseElement->getContent()
        );
    }
}
