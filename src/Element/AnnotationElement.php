<?php

declare(strict_types=1);

namespace Phpro\ApiElementParser\Element;

use Phpro\RefractParser\Element\BaseElement;
use Phpro\RefractParser\Exception\AttributeParserException;
use Phpro\RefractParser\Exception\ContentParserException;
use Phpro\RefractParser\Element\Primitive\ArrayElement;
use Phpro\RefractParser\Element\Primitive\NumberElement;

/**
 * Class AnnotationElement
 *
 * @package Phpro\ApiElementParser\Element
 */
final class AnnotationElement extends BaseElement
{

    public const ELEMENT = 'annotation';

    /**
     * AnnotationElement constructor.
     *
     * @param BaseElement $baseElement
     *
     * @throws \Phpro\RefractParser\Exception\ParserException
     * @throws \Phpro\RefractParser\Exception\AttributeParserException
     * @throws \Phpro\RefractParser\Exception\ContentParserException
     */
    public function __construct(BaseElement $baseElement)
    {
        $attributes = $baseElement->getAttributes();
        $content = $baseElement->getContent();

        if ($attributes->hasAttribute('code') &&
            !$attributes->getAttribute(
                    'code'
                ) instanceof NumberElement) {
            throw new AttributeParserException(
                self::class,
                'code',
                false,
                ['NumberElement']
            );
        }

        if ($attributes->hasAttribute(
                'sourceMap'
            ) && !$attributes->getAttribute(
                    'sourceMap'
                ) instanceof ArrayElement) {
            throw new AttributeParserException(
                self::class,
                'sourceMap',
                false,
                ['ArrayElement']
            );
        }

        if (!\is_string($content->getValue())) {
            throw new ContentParserException(
                self::class,
                $content->getValue(),
                ['string']
            );
        }

        parent::__construct(
            self::ELEMENT,
            $baseElement->getMeta(),
            $baseElement->getAttributes(),
            $baseElement->getContent()
        );
    }
}
