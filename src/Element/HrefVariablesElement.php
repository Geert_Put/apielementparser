<?php

declare(strict_types=1);

namespace Phpro\ApiElementParser\Element;

use Phpro\RefractParser\Element\Primitive\ObjectElement;

/**
 * Class HrefVariablesElement
 *
 * @package Phpro\ApiElementParser\Element
 */
final class HrefVariablesElement extends ObjectElement
{
    public const ELEMENT = 'hrefVariables';
}
