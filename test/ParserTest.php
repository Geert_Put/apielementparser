<?php

declare(strict_types=1);

namespace Phpro\ApiElementParser\Test;

use Phpro\ApiElementParser\Element\AnnotationElement;
use Phpro\ApiElementParser\Element\AssetElement;
use Phpro\ApiElementParser\Element\BasicAuthenticationSchemeElement;
use Phpro\ApiElementParser\Element\CategoryElement;
use Phpro\ApiElementParser\Element\CopyElement;
use Phpro\ApiElementParser\Element\DataStructureElement;
use Phpro\ApiElementParser\Element\ExtensionElement;
use Phpro\ApiElementParser\Element\HrefVariablesElement;
use Phpro\ApiElementParser\Element\HttpHeadersElement;
use Phpro\ApiElementParser\Element\HttpRequestElement;
use Phpro\ApiElementParser\Element\HttpResponseElement;
use Phpro\ApiElementParser\Element\HttpTransactionElement;
use Phpro\ApiElementParser\Element\OAuth2SchemeElement;
use Phpro\ApiElementParser\Element\ParseResultElement;
use Phpro\ApiElementParser\Element\ResourceElement;
use Phpro\ApiElementParser\Element\SourceMapElement;
use Phpro\ApiElementParser\Element\TokenAuthenticationSchemeElement;
use Phpro\ApiElementParser\Element\TransitionElement;
use Phpro\ApiElementParser\Parser;
use Phpro\RefractParser\Element\BaseElement;
use Phpro\RefractParser\Element\Primitive\MemberElement;
use Phpro\RefractParser\Element\Primitive\StringElement;
use PHPUnit\Framework\TestCase;

/**
 * Class ParserTest
 *
 * @package Phpro\RefractParser\Test
 */
final class ParserTest extends TestCase
{

    public function testParseCustomElement()
    {
        $jsonString = <<<'EOL'
        {
          "element": "a custom name"
        }
EOL;
        $result = Parser::parse($jsonString);

        $this->assertInstanceOf(BaseElement::class, $result);
        $this->assertEquals('a custom name', $result->getElement());
    }

    public function testParseHrefVariablesElement()
    {
        $jsonString = <<<'EOL'
        {
          "element": "hrefVariables",
          "content": [
            {
              "element": "member",
              "content": {
                "key": {
                  "element": "string",
                  "content": "question_id"
                }
              }
            }
          ]
        }
EOL;
        $result = Parser::parse($jsonString);

        $this->assertInstanceOf(HrefVariablesElement::class, $result);
        $this->assertEquals('hrefVariables', $result->getElement());
        $this->assertInternalType('array', $result->getContent()->getValue());
        $this->assertContainsOnlyInstancesOf(
            MemberElement::class,
            $result->getContent()->getValue()
        );
    }

    public function testParseDataStructureElement()
    {
        $jsonString = <<<'EOL'
        {
          "element": "dataStructure"
        }
EOL;
        $result = Parser::parse($jsonString);

        $this->assertInstanceOf(DataStructureElement::class, $result);
        $this->assertEquals('dataStructure', $result->getElement());
    }

    public function testParseAssetElement()
    {
        $jsonString = <<<'EOL'
        {
          "element": "asset",
          "meta": {
            "classes": {
              "element": "array",
              "content": [
                {
                  "element": "string",
                  "content": "messageBody"
                }
              ]
            }
          }
        }
EOL;
        $result = Parser::parse($jsonString);

        $this->assertInstanceOf(AssetElement::class, $result);
        $this->assertEquals('asset', $result->getElement());
        $this->assertNotNull($result->getMeta()->getClasses());
        $this->assertContainsOnlyInstancesOf(
            StringElement::class,
            $result->getMeta()->getClasses()->getContent()->getValue()
        );
    }

    public function testParseResourceElement()
    {
        $jsonString = <<<'EOL'
    {
      "element": "resource",
      "meta": {
        "title": {
          "element": "string",
          "content": "Question"
        },
        "description": {
          "element": "string",
          "content": "A Question object has the following attributes."
        }
      },
      "attributes": {
        "href": {
          "element": "string",
          "content": "/questions/{question_id}"
        },
        "hrefVariables": {
          "element": "hrefVariables",
          "content": [
            {
              "element": "member",
              "content": {
                "key": {
                  "element": "string",
                  "content": "question_id"
                }
              }
            }
          ]
        }
      },
      "content": [
        {
          "element": "dataStructure"
        }
      ]
    }
EOL;
        $result = Parser::parse($jsonString);

        $this->assertInstanceOf(ResourceElement::class, $result);
        $this->assertEquals('resource', $result->getElement());
    }

    public function testParseTransitionElement()
    {
        $jsonString = <<<'EOL'
    {
      "element": "transition",
      "attributes": {
        "relation": {
          "element": "string",
          "content": "update"
        },
        "href": {
          "element": "string",
          "content": "https://polls.apiblueprint.org/questions/{question_id}"
        }
      },
      "content": []
    }
EOL;
        $result = Parser::parse($jsonString);

        $this->assertInstanceOf(TransitionElement::class, $result);
        $this->assertEquals('transition', $result->getElement());
        $this->assertInternalType('array', $result->getContent()->getValue());
        $this->assertInstanceOf(
            StringElement::class,
            $result->getAttributes()->getAttribute('relation')
        );
        $this->assertInstanceOf(
            StringElement::class,
            $result->getAttributes()->getAttribute('href')
        );

    }

    public function testParseCategoryElement()
    {
        $jsonString = <<<'EOL'
    {
      "element": "category",
      "meta": {
        "classes": {
          "element": "array",
          "content": [
            {
              "element": "string",
              "content": "api"
            }
          ]
        },
        "title": {
          "element": "string",
          "content": "Polls API"
        }
      },
      "attributes": {
        "metadata": {
          "element": "array",
          "content": [
            {
              "element": "member",
              "meta": {
                "classes": {
                  "element": "array",
                  "content": [
                    {
                      "element": "string",
                      "content": "user"
                    }
                  ]
                }
              },
              "content": {
                "key": {
                  "element": "string",
                  "content": "HOST"
                },
                "value": {
                  "element": "string",
                  "content": "http://polls.apiblueprint.org/"
                }
              }
            }
          ]
        }
      },
      "content": [
        {
          "element": "category",
          "meta": {
            "classes": {
              "element": "array",
              "content": [
                {
                  "element": "string",
                  "content": "resourceGroup"
                }
              ]
            },
            "title": {
              "element": "string",
              "content": "Question"
            }
          },
          "content": [
            {
              "element": "copy",
              "content": "Resources related to questions in the API."
            }
          ]
        }
      ]
    }
EOL;
        $result = Parser::parse($jsonString);

        $this->assertInstanceOf(CategoryElement::class, $result);
        $this->assertEquals('category', $result->getElement());
        $this->assertEquals(
            'api',
            $result->getMeta()->getClasses()->getContent()->getValue(
            )[0]->getContent()->getValue()
        );
    }

    public function testParseCopyElement()
    {
        $jsonString = <<<'EOL'
    {
      "element": "copy",
      "content": "Lorem Ipsum"
    }
EOL;
        $result = Parser::parse($jsonString);

        $this->assertInstanceOf(CopyElement::class, $result);
        $this->assertEquals('copy', $result->getElement());
    }

    public function testParseHttpTransactionElement()
    {
        $jsonString = <<<'EOL'
    {
      "element": "httpTransaction",
      "content": [
        {
          "element": "httpRequest",
          "attributes": {
            "method": {
              "element": "string",
              "content": "GET"
            },
            "href": {
              "element": "string",
              "content": "/questions/{question_id}"
            },
            "hrefVariables": {
              "element": "hrefVariables",
              "content": [
                {
                  "element": "member",
                  "content": {
                    "key": {
                      "element": "string",
                      "content": "question_id"
                    }
                  }
                }
              ]
            }
          },
          "content": []
        },
        {
          "element": "httpResponse",
          "attributes": {
            "statusCode": {
              "element": "number",
              "content": 200
            }
          },
          "content": [
            {
              "element": "asset",
              "meta": {
                "classes": {
                  "element": "array",
                  "content": [
                    {
                      "element": "string",
                      "content": "messageBody"
                    }
                  ]
                }
              },
              "attributes": {
                "contentType": {
                  "element": "string",
                  "content": "application/json"
                }
              },
              "content": "{\"name\": \"John\"}"
            }
          ]
        }
      ]
    }
EOL;
        $result = Parser::parse($jsonString);

        $this->assertInstanceOf(HttpTransactionElement::class, $result);
        $this->assertEquals('httpTransaction', $result->getElement());
    }

    public function testParseHttpHeadersElement()
    {
        $jsonString = <<<'EOL'
    {
      "element": "httpHeaders",
      "content": [
        {
          "element": "member",
          "content": {
            "key": {
              "element": "string",
              "content": "Content-Type"
            },
            "value": {
              "element": "string",
              "content": "application/json"
            }
          }
        }
      ]
    }
EOL;
        $result = Parser::parse($jsonString);

        $this->assertInstanceOf(HttpHeadersElement::class, $result);
        $this->assertEquals('httpHeaders', $result->getElement());
        $this->assertContainsOnlyInstancesOf(
            MemberElement::class,
            $result->getContent()->getValue()
        );
    }

    public function testParseHttpRequestElement()
    {
        $jsonString = <<<'EOL'
    {   
    "element": "httpRequest",
      "attributes": {
        "method": {
        "element": "string",
          "content": "GET"
        },
        "href": {
        "element": "string",
          "content": "/questions/{question_id}"
        },
        "hrefVariables": {
        "element": "hrefVariables",
          "content": [
            {
                "element": "member",
              "content": {
                "key": {
                    "element": "string",
                  "content": "question_id"
                }
              }
            }
          ]
        }
      }
     }
EOL;
        $result = Parser::parse($jsonString);

        $this->assertInstanceOf(HttpRequestElement::class, $result);
        $this->assertEquals('httpRequest', $result->getElement());
    }

    public function testParseHttpResponseElement()
    {
        $jsonString = <<<'EOL'
    {
      "element": "httpResponse",
      "attributes": {
        "statusCode": {
          "element": "number",
          "content": 200
        }
      },
      "content": [
        {
          "element": "asset",
          "meta": {
            "classes": {
              "element": "array",
              "content": [
                {
                  "element": "string",
                  "content": "messageBody"
                }
              ]
            }
          },
          "attributes": {
            "contentType": {
              "element": "string",
              "content": "application/json"
            }
          },
          "content": "{\"name\": \"John\"}"
        }
      ]
    }
EOL;
        $result = Parser::parse($jsonString);

        $this->assertInstanceOf(HttpResponseElement::class, $result);
        $this->assertEquals('httpResponse', $result->getElement());
    }

    public function testParseParseResultElement()
    {
        $jsonString = <<<'EOL'
    {
      "element": "parseResult",
      "content": [
        {
          "element": "category",
          "meta": {
            "classes": {
              "element": "array",
              "content": [
                {
                  "element": "string",
                  "content": "api"
                }
              ]
            }
          }
        },
        {
          "element": "annotation",
          "meta": {
            "classes": {
              "element": "array",
              "content": [
                {
                  "element": "string",
                  "content": "warning"
                }
              ]
            }
          },
          "attributes": {
            "code": {
              "element": "number",
              "content": 6
            },
            "sourceMap": {
              "element": "array",
              "content": [
                {
                  "element": "sourceMap",
                  "content": [
                    {
                      "element": "array",
                      "content": [
                        {
                          "element": "number",
                          "content": 0
                        },
                        {
                          "element": "number",
                          "content": 9
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          },
          "content": "action"
        }
      ]
    }
EOL;
        $result = Parser::parse($jsonString);

        $this->assertInstanceOf(ParseResultElement::class, $result);
        $this->assertEquals('parseResult', $result->getElement());
    }

    public function testParseAnnotationElement()
    {
        $jsonString = <<<'EOL'
    {
      "element": "annotation",
      "meta": {
        "classes": {
          "element": "array",
          "content": [
            {
              "element": "string",
              "content": "warning"
            }
          ]
        }
      },
      "attributes": {
        "code": {
          "element": "number",
          "content": 6
        },
        "sourceMap": {
          "element": "array",
          "content": [
            {
              "element": "sourceMap",
              "content": [
                {
                  "element": "array",
                  "content": [
                    {
                      "element": "number",
                      "content": 4
                    },
                    {
                      "element": "number",
                      "content": 12
                    }
                  ]
                },
                {
                  "element": "array",
                  "content": [
                    {
                      "element": "number",
                      "content": 20
                    },
                    {
                      "element": "number",
                      "content": 12
                    }
                  ]
                }
              ]
            }
          ]
        }
      },
      "content": "action is missing a response"
    }
EOL;
        $result = Parser::parse($jsonString);

        $this->assertInstanceOf(AnnotationElement::class, $result);
        $this->assertEquals('annotation', $result->getElement());
    }

    public function testParseSourceMapElement()
    {
        $jsonString = <<<'EOL'
    {
      "element": "sourceMap",
      "content": [
        {
          "element": "array",
          "content": [
            {
              "element": "number",
              "content": 4
            },
            {
              "element": "number",
              "content": 12
            }
          ]
        },
        {
          "element": "array",
          "content": [
            {
              "element": "number",
              "content": 4
            },
            {
              "element": "number",
              "content": 12
            }
          ]
        }
      ]
    }
EOL;
        $result = Parser::parse($jsonString);

        $this->assertInstanceOf(SourceMapElement::class, $result);
        $this->assertEquals('sourceMap', $result->getElement());
    }


    public function testParseBasicAuthenticationSchemeElement()
    {
        $jsonString = <<<'EOL'
        {
          "element": "Basic Authentication Scheme",
          "meta": {
            "id": {
              "element": "string",
              "content": "Custom Basic Auth"
            }
          },
          "content": [
            {
              "element": "member",
              "content": {
                "key": {
                  "element": "string",
                  "content": "username"
                },
                "value": {
                  "element": "string",
                  "content": "john.doe"
                }
              }
            },
            {
              "element": "member",
              "content": {
                "key": {
                  "element": "string",
                  "content": "password"
                },
                "value": {
                  "element": "string",
                  "content": "1234password"
                }
              }
            }
          ]
        }
EOL;
        $result = Parser::parse($jsonString);

        $this->assertInstanceOf(BasicAuthenticationSchemeElement::class, $result);
        $this->assertEquals('Basic Authentication Scheme', $result->getElement());
    }

    public function testParseTokenAuthenticationSchemeElement()
    {
        $jsonString = <<<'EOL'
        {
          "element": "Token Authentication Scheme",
          "meta": {
            "id": {
              "element": "string",
              "content": "Custom Token Auth"
            }
          },
          "content": [
            {
              "element": "member",
              "content": {
                "key": {
                  "element": "string",
                  "content": "queryParameterName"
                },
                "value": {
                  "element": "string",
                  "content": "api_key"
                }
              }
            }
          ]
        }
EOL;
        $result = Parser::parse($jsonString);

        $this->assertInstanceOf(TokenAuthenticationSchemeElement::class, $result);
        $this->assertEquals('Token Authentication Scheme', $result->getElement());
    }

    public function testParseOAuth2SchemeElement()
    {
        $jsonString = <<<'EOL'
        {
          "element": "OAuth2 Scheme",
          "meta": {
            "id": {
              "element": "string",
              "content": "Custom OAuth2"
            }
          },
          "content": [
            {
              "element": "member",
              "content": {
                "key": {
                  "element": "string",
                  "content": "scopes"
                },
                "value": {
                  "element": "array",
                  "content": [
                    {
                      "element": "string",
                      "content": "scope1"
                    },
                    {
                      "element": "string",
                      "content": "scope2"
                    }
                  ]
                }
              }
            },
            {
              "element": "member",
              "content": {
                "key": {
                  "element": "string",
                  "content": "grantType"
                },
                "value": {
                  "element": "string",
                  "content": "implicit"
                }
              }
            },
            {
              "element": "transition",
              "attributes": {
                "relation": {
                  "element": "string",
                  "content": "authorize"
                },
                "href": {
                  "element": "string",
                  "content": "/authorize"
                }
              }
            },
            {
              "element": "transition",
              "attributes": {
                "relation": {
                  "element": "string",
                  "content": "token"
                },
                "href": {
                  "element": "string",
                  "content": "/token"
                }
              }
            }
          ]
        }
EOL;
        $result = Parser::parse($jsonString);

        $this->assertInstanceOf(OAuth2SchemeElement::class, $result);
        $this->assertEquals('OAuth2 Scheme', $result->getElement());
    }

    public function testParseExtensionElement()
    {
        $jsonString = <<<'EOL'
        {
          "element": "extension",
          "meta": {
            "links": {
              "element": "array",
              "content": [
                {
                  "element": "link",
                  "attributes": {
                    "relation": {
                      "element": "string",
                      "content": "profile"
                    },
                    "href": {
                      "element": "string",
                      "content": "http://example.com/extensions/info/"
                    }
                  }
                }
              ]
            }
          },
          "content": {
            "element": "object",
            "content": [
              {
                "element": "member",
                "content": {
                  "key": {
                    "element": "string",
                    "content": "version"
                  },
                  "value": {
                    "element": "string",
                    "content": "1.0"
                  }
                }
              }
            ]
          }
        }
EOL;
        $result = Parser::parse($jsonString);

        $this->assertInstanceOf(ExtensionElement::class, $result);
        $this->assertEquals('extension', $result->getElement());
    }
}
