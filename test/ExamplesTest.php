<?php

declare(strict_types=1);

namespace Phpro\ApiElementParser\Test;

use Phpro\ApiElementParser\Element\ParseResultElement;
use Phpro\ApiElementParser\Parser;
use PHPUnit\Framework\TestCase;

/**
 * Class ParserTest
 *
 * @package Phpro\RefractParser\Test
 */
final class ExamplesTest extends TestCase
{

    public function testParseExamplesApiElements()
    {
        $files = scandir('test/examples/api_elements', SCANDIR_SORT_NONE);
        foreach ($files as $file) {
            if (!(substr($file, -5) === '.json')) {
                continue;
            }

            $jsonString = file_get_contents(sprintf('test/examples/api_elements/%s', $file));
            $result = Parser::parse($jsonString);

            $this->assertInstanceOf(ParseResultElement::class, $result);
        }
    }

    public function testParseExamplesApiBlueprint()
    {
        $files = scandir('test/examples/api_blueprint', SCANDIR_SORT_NONE);
        foreach ($files as $file) {
            if (!(substr($file, -5) === '.json')) {
                continue;
            }

            $jsonString = file_get_contents(sprintf('test/examples/api_blueprint/%s', $file));
            $result = Parser::parse($jsonString);

            $this->assertInstanceOf(ParseResultElement::class, $result);
        }
    }
}
